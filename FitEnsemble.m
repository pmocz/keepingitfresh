%% Random Forest algorithm
% see http://www.mathworks.com/help/stats/ensemble-methods.html#br0gr2o
% about optimizing

clc;
clear all;
close all;
rng(47);


%% load data
train_tfidf = 0;%hdf5read('features/train_tfidf.h5','/dataset')';
test1_tfidf = 0;%hdf5read('features/test1_tfidf.h5','/dataset')';
train_rating = hdf5read('features/train_rating.h5','/dataset')';
test1_rating = hdf5read('features/test1_rating.h5','/dataset')';
train_business = hdf5read('features/train_rating.h5','/dataset')';
test1_business = hdf5read('features/test1_rating.h5','/dataset')';
train_vh = hdf5read('features/train_VH.h5','/dataset')';
test1vh = hdf5read('features/test1_VH.h5','/dataset')';

train_violations = csvread('data/train_labels.csv',1,3);
fid = fopen('data/SubmissionFormat.csv');
test1_labels = textscan(fid, '%s %s %s %s %s %s', 'delimiter',',');
fclose(fid);


%% set features
%train_features = train_tfidf;
%test1features = test1_tfidf;

%train_features = [train_tfidf train_rating train_business];
%test1features = [test1_tfidf test1_rating test1_business];

train_vh(:,end-2:end) = train_vh(:,end-2:end)/100;
test1vh(:,end-2:end) = test1vh(:,end-2:end)/100;

train_features = double([train_vh]);
test1features = double([test1vh]);

%% Model
Nlearn = 100; 300;
X = train_features;
Y = train_violations;

disp('E1')
BS = fitensemble(X,Y(:,1),'LSBoost',Nlearn,'Tree');
disp('E2')
BSS = fitensemble(X,Y(:,2),'LSBoost',Nlearn,'Tree');
disp('E3')
BSSS = fitensemble(X,Y(:,3),'LSBoost',Nlearn,'Tree');


%% Predictions

PXS = predict(BS,test1features);
PXSS = predict(BSS,test1features);
PXSSS = predict(BSSS,test1features);

PXS = round(PXS);
PXSS = round(PXSS);
PXSSS = round(PXSSS);

PXS(PXS<0)=0;
PXSS(PXSS<0)=0;
PXSSS(PXSSS<0)=0;

%% Write file

fid = fopen('FitEnsemblePhaseI.csv','wt');
rows = length(test1_labels{1});
L1 = test1_labels{1}(1);
L2 = test1_labels{2}(1);
L3 = test1_labels{3}(1);
fprintf(fid, '%s,%s,%s,*,**,***\n', L1{1}, L2{1}, L3{1});
for i=2:rows
    L1 = test1_labels{1}(i);
    L2 = test1_labels{2}(i);
    L3 = test1_labels{3}(i);
    fprintf(fid, '%s,%s,%s,%d,%d,%d', L1{1}, L2{1}, L3{1}, PXS(i-1), PXSS(i-1), PXSSS(i-1));
    fprintf(fid, '\n');
end
fclose(fid);

