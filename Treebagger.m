%% Random Forest algorithm
% see http://www.mathworks.com/help/stats/ensemble-methods.html#br0gr2o
% about optimizing

clc;

clear all;
close all;
rng(47);


%% load data
train_tfidf = hdf5read('features/train_tfidfLAG2A.h5','/dataset')';
test1_tfidf = hdf5read('features/test1_tfidfLAG2.h5','/dataset')';
test2_tfidf = hdf5read('features/test2_tfidfLAG2.h5','/dataset')';
train_rating = hdf5read('features/train_ratingA.h5','/dataset')';
test1_rating = hdf5read('features/test1_rating.h5','/dataset')';
test2_rating = hdf5read('features/test2_rating.h5','/dataset')';
train_business = hdf5read('features/train_businessA.h5','/dataset')';
test1_business = hdf5read('features/test1_business.h5','/dataset')';
test2_business = hdf5read('features/test2_business.h5','/dataset')';
train_vh = hdf5read('features/train_VH4A.h5','/dataset')';
test1_vh = hdf5read('features/test1_VH4.h5','/dataset')';
test2_vh = hdf5read('features/test2_VH4.h5','/dataset')';

%train_violations = csvread('data/train_labels.csv',1,3);
train_violations = csvread('data/AllViolations.csv',1,3);  % use all data for final model fitting
fid = fopen('data/SubmissionFormat.csv');
test1_labels = textscan(fid, '%s %s %s %s %s %s', 'delimiter',',');
fclose(fid);
fid = fopen('data/PhaseIISubmissionFormat.csv');
test2_labels = textscan(fid, '%s %s %s %s %s %s', 'delimiter',',');
fclose(fid);


%% test 2 features need to be repeated 43 times, lag adjusted

N_test2 = length(test2_vh);
test2_tfidf = repmat(test2_tfidf,[43,1]);
test2_rating = repmat(test2_rating,[43,1]);
test2_business = repmat(test2_business,[43,1]);
test2_vh = repmat(test2_vh,[43,1]);

date_shift = 0.01*repmat(0:42,[N_test2,1]);
date_shift = date_shift(:);
date_shift =  repmat(date_shift,[1,4]);
test2_vh(:,[10 11 12 16]) = test2_vh(:,[10 11 12 16]) + date_shift;


%% set features
train_features = train_tfidf;
test1_features = test1_tfidf;

train_features = double([train_business train_rating]);
test1_features = double([test1_business test1_rating]);

train_features = double([train_business train_rating train_vh]);
test1_features = double([test1_business test1_rating test1_vh]);

% quick! - for testing
train_features = double(train_vh);
test1_features = double(test1_vh);
test2_features = double(test2_vh);

% for final model !!!
train_features = double([train_business train_vh train_rating train_tfidf]);
test1_features = double([test1_business test1_vh test1_rating test1_tfidf]);
test2_features = double([test2_business test2_vh test2_rating test2_tfidf]);

Ncat = size(train_business);
Ncat = Ncat(2);
Nfeatures = size(train_features);
Nfeatures = Nfeatures(2);

iscat = zeros(1,Nfeatures);
iscat(1:Ncat) = 1;
iscat = logical(iscat);




%% Model

NTrees = 80; 70; 60; 50; 200; 50; 20;
MinLeaf = 5;
X = train_features;
Y = train_violations;

% use this !!!
disp('TreeBagger1')
BS = TreeBagger(NTrees,X,Y(:,1),'method','regression','MinLeaf',MinLeaf,'CategoricalPredictors',iscat);
disp('TreeBagger2')
BSS = TreeBagger(NTrees,X,Y(:,2),'method','regression','MinLeaf',MinLeaf,'CategoricalPredictors',iscat);
disp('TreeBagger3')
BSSS = TreeBagger(NTrees,X,Y(:,3),'method','regression','MinLeaf',MinLeaf,'CategoricalPredictors',iscat);

% for non-categorial data - faster
% disp('TreeBagger1')
% BS = TreeBagger(NTrees,X,Y(:,1),'method','regression','MinLeaf',MinLeaf);
% disp('TreeBagger2')
% BSS = TreeBagger(NTrees,X,Y(:,2),'method','regression','MinLeaf',MinLeaf);
% disp('TreeBagger3')
% BSSS = TreeBagger(NTrees,X,Y(:,3),'method','regression','MinLeaf',MinLeaf);


%% Predictions

PXS = predict(BS,test1_features);
PXSS = predict(BSS,test1_features);
PXSSS = predict(BSSS,test1_features);

PXS = round(PXS);
PXSS = round(PXSS);
PXSSS = round(PXSSS);

PXS(PXS<0)=0;
PXSS(PXSS<0)=0;
PXSSS(PXSSS<0)=0;



%% Predictions 2

P2XS = predict(BS,test2_features);
P2XSS = predict(BSS,test2_features);
P2XSSS = predict(BSSS,test2_features);

P2XS = round(P2XS);
P2XSS = round(P2XSS);
P2XSSS = round(P2XSSS);

P2XS(P2XS<0)=0;
P2XSS(P2XSS<0)=0;
P2XSSS(P2XSSS<0)=0;


%% Write file

fid = fopen('TreeBaggerPhaseI.csv','wt');
rows = length(test1_labels{1});
L1 = test1_labels{1}(1);
L2 = test1_labels{2}(1);
L3 = test1_labels{3}(1);
fprintf(fid, '%s,%s,%s,*,**,***\n', L1{1}, L2{1}, L3{1});
for i=2:rows
    L1 = test1_labels{1}(i);
    L2 = test1_labels{2}(i);
    L3 = test1_labels{3}(i);
    fprintf(fid, '%s,%s,%s,%d,%d,%d', L1{1}, L2{1}, L3{1}, PXS(i-1), PXSS(i-1), PXSSS(i-1));
    fprintf(fid, '\n');
end
fclose(fid);


%% Write file 2

fid = fopen('TreeBaggerPhaseII.csv','wt');
rows = length(test2_labels{1});
L1 = test2_labels{1}(1);
L2 = test2_labels{2}(1);
L3 = test2_labels{3}(1);
fprintf(fid, '%s,%s,%s,*,**,***\n', L1{1}, L2{1}, L3{1});
for i=2:rows
    L1 = test2_labels{1}(i);
    L2 = test2_labels{2}(i);
    L3 = test2_labels{3}(i);
    fprintf(fid, '%s,%s,%s,%d,%d,%d', L1{1}, L2{1}, L3{1}, P2XS(i-1), P2XSS(i-1), P2XSSS(i-1));
    fprintf(fid, '\n');
end
fclose(fid);


%% save the models

mS = compact(BS);
mSS = compact(BSS);
mSSS = compact(BSSS);
save mS.mat mS
save mSS.mat mSS
save mSSS.mat mSSS



